QHangman
----------------------

You need Qt5 and CMake 3 or higher

-- Build instructions --

```
git clone https://gitlab.com/Petross404/QHangman.git
cd path/to/QHangman
mkdir build && cd build
cmake .. && make

#Run the application
./qhangman
```
